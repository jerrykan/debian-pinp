# Podman in Podman
A Debian-based container image that supports running rootless containers inside
a rootless container using `podman`. Useful for do image builds within a
container.

## Requirements
### Host
This image works on a Debian 11/bullseye system with the following package
versions:

  - `podman` (3.0.1+dfsg1-3+deb11u1)

  - `libseccomp2` (2.5.1-1+deb11u1)
    - podman v4.1.0 (used in the container image) uses the `range_close()`
      syscall which isn't supported in older versions of `libseccomp2`.

  - `linux-image-amd64` (5.18.2-1~bpo11+1)
    - a "recent" version of the kernel (at least v5.14) is required so that
      volumes in a container within this container work.
    - using and older version of the kernel will result in errors like the
      following when trying to run podman inside a podman container:

        ```
        error running container: error from /usr/bin/crun creating container for [/bin/sh -c mkdir -p       /var/lib/shared/overlay-images       /var/lib/shared/overlay-layers       /var/lib/shared/vfs-images       /var/lib/shared/vfs-layers  && touch /var/lib/shared/overlay-images/images.lock  && touch /var/lib/shared/overlay-layers/layers.lock  && touch /var/lib/shared/vfs-images/images.lock  && touch /var/lib/shared/vfs-layers/layers.lock]: mount `/home/gitlab/.local/share/containers/storage/overlay-containers/18261f5dbdba9bceb1831e597adddfe8d4f14704456c75317ad8904b1d87e7da/userdata/overlay/74810361/merge` to '/home/gitlab/.local/share/containers': Operation not permitted
        : exit status 1
        Error: error building at STEP "RUN mkdir -p       /var/lib/shared/overlay-images       /var/lib/shared/overlay-layers       /var/lib/shared/vfs-images       /var/lib/shared/vfs-layers  && touch /var/lib/shared/overlay-images/images.lock  && touch /var/lib/shared/overlay-layers/layers.lock  && touch /var/lib/shared/vfs-images/images.lock  && touch /var/lib/shared/vfs-layers/layers.lock": error while running runtime: exit status 1
        ```

The provided [`Vagrantfile`](Vagrantfile) can be used to provision a VM, using
[Vagrant](https://www.vagrantup.com/), that is capable of running a container
inside this container image. Just be sure to run `vagrant reload` before
running `vagrant ssh` (to ensure the correct kernel version is running).

### Image
The image uses podman v4.1.0. Earlier version result in the following error
when trying to build an image:

    mount `/proc` to '/proc': Operation not permitted


## Testing
Building this image inside a container provides a reasonably good test to see
if image works as expected.

### Build the image on the host
Run the following commands to do an initial build of the container:

    $ podman build -t pinp .

### Build the image inside a container
Run the image build again inside of a container based on the newly create
image:

    $ podman run -ti --rm --device /dev/fuse -v .:/src/ pinp podman build /src/


## References
This image is based on the example
[podman](https://quay.io/repository/podman/stable) image from upstream:

  - https://github.com/containers/podman/blob/main/contrib/podmanimage/stable/Containerfile

The following blog post provides a bit more context about running podman inside
a container:

  - https://www.redhat.com/sysadmin/podman-inside-container
