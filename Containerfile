FROM debian:11

RUN echo 'deb http://deb.debian.org/debian experimental main' \
        > /etc/apt/sources.list.d/experimental.list \
 && apt-get update \
 && apt-get upgrade -y \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ca-certificates \
        fuse-overlayfs \
        libcap2-bin \
        podman \
        systemd \
        uidmap \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends -t experimental \
        podman \
 && rm /etc/apt/sources.list.d/experimental.list \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# Fix caps of newuidmap/newgidmap
RUN chmod 755 /usr/bin/newuidmap \
 && chmod 755 /usr/bin/newgidmap \
 && setcap cap_setuid=ep /usr/bin/newuidmap \
 && setcap cap_setgid=ep /usr/bin/newgidmap

# We need to keep subuid/subgid range within the subuid/subgid range of the
# user running the container (ie. 0-65535).
RUN useradd podman --shell /bin/bash \
 && echo "podman:1:999\npodman:1001:64535" > /etc/subuid \
 && echo "podman:1:999\npodman:1001:64535" > /etc/subgid

# From here down is based on:
#   https://github.com/containers/podman/blob/main/contrib/podmanimage/stable/Containerfile
ADD rootfs/  /

RUN mkdir -p /home/podman/.local/share/containers \
 && chown podman:podman -R /home/podman \
 && chmod 644 /etc/containers/containers.conf

VOLUME /var/lib/containers
VOLUME /home/podman/.local/share/containers

RUN mkdir -p \
      /var/lib/shared/overlay-images \
      /var/lib/shared/overlay-layers \
      /var/lib/shared/vfs-images \
      /var/lib/shared/vfs-layers \
 && touch /var/lib/shared/overlay-images/images.lock \
 && touch /var/lib/shared/overlay-layers/layers.lock \
 && touch /var/lib/shared/vfs-images/images.lock \
 && touch /var/lib/shared/vfs-layers/layers.lock

ENV _CONTAINERS_USERNS_CONFIGURED=""

USER podman
